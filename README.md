# Slides : Gestion de projet informatique

Slides constitués pour le séminaire SéPAG du 24 Mai 2023.

## Compilation
```
typst c main.typ output/main.pdf
```

## Crédits
- [typst](https://github.com/typst/typst)
- [typst-slides](https://github.com/andreasKroepelin/typst-slides)