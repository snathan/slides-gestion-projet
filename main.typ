#import "slides.typ": *   

#show: slides.with(
    authors: "Solal Nathan",
    short-authors: "Solal Nathan",
    title: [Gestion de projet informatique],
    subtitle: "",
    short-title: "Gestion projet informatique",
    date: "April 2023",
)

#show link: set text(blue)
#set text(font: "Inria Sans")

#slide(theme-variant: "title slide")

#new-section("Introduction")

#slide(title: "Pourquoi ?")[
    Quand on fait de la recherche en informatique (et dans pleins d'autres domaines d'ailleurs) on produit souvent du *code*.

    On peut s'inpirer des commaunutés open source qui maintienne des grandes code-base pour avoir des bonnes idées.
]

#slide(title: "Visé de la présentation")[
    - Pas un cours de transmission de connaissance ;
    - Lancer des pistes de réflection ;
    - Découvrir des choses, susciter la curiosité ;
    - Partager des idées et des ressources entre nous.
]

#slide(title: "Git")[

    #grid(
        columns: (2fr, 1fr),
        [
        #link("https://git-scm.com/")[git] est un logiciel de *gestion de version* decentralisé.

        Pour la petite histoire créer par Linus Torvarld en 2005 (autour du noyau Linux).
    ],
    [
        #image("assets/git.png", width: 100%)
        #image("assets/git_session.png")
    ],
    )
]

#let two_columns_slide(content1, content2, title: "", col1: 1fr, col2: 1fr, gutter: auto) = {
    slide(title: title)[
        #grid(
            columns: (col1, col2),
            column-gutter: gutter,
            content1,
            content2,
        )
    ]
}

#two_columns_slide(
    title: "Git : Cheat Sheet",
    col1: 3fr,
    col2: 1fr,
    [
    - `git config --global user.name "[name]"`
    - `git config --global user.email "[email]"`  
    - `git init`
    - `git clone [url]`
    - `git branch [branch]`
    - `git checkout [branch]`
    - `git merge [branch]` (et stratégie de résolution de merge conflict, voir `rebase`)
    - `git add -p` (hunk)
    ],
    [
        #link("https://training.github.com/downloads/github-git-cheat-sheet.pdf")[Exemple de Cheat Sheet]
    ]
)

#two_columns_slide(
    title: "Git : Ressource",
    col1: 1fr,
    col2: 1fr,
    [
        #image("assets/ohshitgit.png", height: 70%)
    ],
    [
        #image("assets/ohshitgit2.png", height:60%)
        #link("https://ohshitgit.com/")[ohshitgit.com]
    ]
)


#two_columns_slide(
    title: "Commit message convention",
    col1: 1fr,
    col2: 1fr,
    [
        #image("assets/type-scope.png", height: 60%)
        #link("https://www.conventionalcommits.org/en/v1.0.0/")[https://conventionalcommits.org]
    ],
    [
        #image("assets/gitmoji.png")
        #link("https://gitmoji.dev/")[https://gitmoji.dev/]
    ]
)

#slide(title: "Commit message convention")[
    - Les messages de commit sont fait pour être courts (même si c'est pas limité en pratique, à part à `size_t`);
    - Recommendation
        - first line is short (50 characters max)
        - blank line
        - wrapped at 72 then
    - And the footer (par exemple utilisé pour dire qu'on ferme une issue `Closes #76`)
    - fun fact : `AUTHOR` et `COMMITER` c'est pas la même chose
    - vous pouvez signer vos commits avec GPG
]

#slide(title: "Commit message convention")[
```
fix: fix foo to enable bar

This fixes the broken behavior of the component by doing xyz. 

BREAKING CHANGE
Before this fix foo wasn't enabled at all, behavior changes from <old> to <new>

Closes #76
```
]

#slide(title: `.gitignore`)[
Exemple de `.gitignore` minimaliste pour Python.
```bash
# Byte-compiled / optimized / DLL files
__pycache__/
*.py[cod]
*$py.class

# C extensions
*.so

# Jupyter Notebook
.ipynb_checkpoints
```
]

#slide(title: "Note sur les fichiers volumineux")[
    - binary files, datasets, etc
    - `git LFS` peut être une option (à voir sur le serveur le supporte)
    - sinon hébergement externe (Nextcloud du LISN avec un lien de partage en téléchargement seul par exemple)
]

#slide(title: "Semantic Versionning")[
#link("https://semver.org/")[semver.org] exemple: `2.0.1` ou `0.9.8-beta`

Given a version number MAJOR.MINOR.PATCH, increment the:

1. MAJOR version when you make incompatible API changes
2. MINOR version when you add functionality in a backward compatible manner
3. PATCH version when you make backward compatible bug fixes

Additional labels for pre-release and build metadata are available as extensions to the MAJOR.MINOR.PATCH format.
]

#two_columns_slide(
    title: "Structure de fichier",
    col1: 2fr,
    col2: 3fr,
    gutter: 15pt,
    [
        #align(horizon)[
            #image("assets/structure.png")
        ]
    ],
    [
        Il y a quelques structrure de projets classiques

        - `src`, `assets`, `static`, `docs` (concept de monorepo, _cf_ #link("https://monorepo.tools/")[monorepo.tools])
        - `README.md`
        - `LICENSE`
        - `INSTALL` (souvent inclu dans le `README`)
        - `CONTRIBUTING` guidlines for contributing
        - `HACKING`, `BUG`, `SECURITY`, `CHANGELOG` (#text(size: 16pt)[#link("https://github.com/conventional-changelog/conventional-changelog")[conventional-changelog]]), checklists, ...
    ],
)

#two_columns_slide(
    title: "Cookie Cutter Structure",
    col1: 4fr,
    col2: 3fr,
    [
        #h(2cm)

        - #link("https://drivendata.github.io/cookiecutter-data-science/")[Cookie Cutter Data Science] example
    ],
    [ 
        #image("assets/cookie-cutter.png", height: 70%)
    ]
)

#two_columns_slide(
    title: "Licensing",
    col1: 2fr,
    col2: 1fr,
    [
        #image("assets/choose-a-license.png", height: 60%)
        #link("https://choosealicense.com/")[choosealicense.com]
    ],
    [
        - GNU AGPLv3
        - GNU GPLv3
        - GNU LGPLv3
        - Apache 2.0
        - MIT
        - CC BY-SA-NC-ND
    ]
)
#slide(
    title: "Les arcanes des forge logicielles"
)[
    - issues
    - merge requests
    - fork
    - protected branches
]

#slide(
    title: "Commentaire et documentation"
)[
    - écrivez les pour les autres, mais surtout pour vous
    - utilisez des outils et formats standard (docstring)
    - un site web pour une doc est overkill pour des petits projet mais a du sens pour une bibliothèque
]

#slide(
    title: "Bonnes pratiques"
)[
    Il existe pleins de « bonnes pratiques » disponible sur internet.

    Exemple en ML : #link("https://github.com/paperswithcode/releasing-research-code")[Tips for Publishing Research Code] (qui est devenu les guidelines officiels pour NeurIPS)

    - #link("https://github.com/Kristories/awesome-guidelines")[Awesome Guidelines]
]

#slide(
    title: "Linting"
)[
    - tabs vs spaces ? et combien ?
    #uncover(2)[
    - il y a des options de liting pour tout
        - python (pylint, flake8, PYFlakes, Bandit, black, *ruff*, isort, ...)
        - tous les languages : C, caml, JS, Rust, ...
        - bash (shellchecker)
        - html/markdown (prettier, ...)
        - la prose (proselint)
        - les liens morts (linkchecker)
        - ...
    ]
]

#slide(
    title: "Coding styles & docstring"
)[
    - un style unifié rends le projet plus lisible et facile à maintenir
    - peut être inforce par une CI (de façon plus ou moins stricte)
    - pre-commit hook
    - *PEP8* en Python
    - docstring style (Google, Numpy, etc)
        - peut être utilisé par un site static de doc (Sphinx, readthedocs, ...)
]

#slide(
    title: "Testing"
)[
    - code quality
    - coverage
    - shields (_cf_ #link("https://shields.io/")[shields.io])
    #image("assets/shields.png", width: 50%)
]
#slide(
    title: "Refactoring"
)[
    - des règles générales comme : éviter copier-coller du code (boiler plate)
    - code smells
    - #link("https://refactoring.guru/")[refactoring.guru]
]

#slide(
    title: "CI/CD"
)[
    - Intégration Continue (CI)
    - Livraison Continue (CD)
    - CI/CD automatise vos constructions, vos tests et vos déploiements afin que vous puissiez livrer les changements de code plus rapidement et de manière plus fiable.
    - DevOps, Docker, automatic testing, automatic artefact generation, ...
    - Gitlab-CI
]

#slide(
    title: "Note sur la sécurité"
)[
    - utilisez des clé SSH (ed25519 de préférence, sur un token physique en option)
    - 2FA c'est bien
]

#slide(
    title: "Dépendences"
)[
    - _Standing on the shoulder of giants_
    - En avoir trop c'est pas toujours bien (log4j)
    - attention au _dependency rot_ (#text(size: 16pt)[pensez à mettre à jour,] #text(size: 12pt)[en plus les nouvelle features sont cools vous allez voir])
    - `requirements.txt`
    - `virtualenv`
    - `docker`
]

#slide()[
    #h(1fr)
    #align(center)[
    = Merci de votre attention
    ]
]